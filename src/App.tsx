import React, { useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import { AppShell, MantineProvider } from '@mantine/core';
import '@mantine/core/styles.css';

import WeatherStore from './core/store/weather/weather.store';
import { MainScreen } from './screen/MainScreen/MainScreen';

import style from './App.module.scss';
import { City } from './core/api/rest/Openweathermap/openweathermap.model';

const App = observer(() => {
    return (
        <MantineProvider defaultColorScheme="dark">
            <AppShell className={style['app']}>
                <AppShell.Main className={style['app__main']}>
                    <MainScreen />
                </AppShell.Main>
            </AppShell>
        </MantineProvider>
    );
});

export default App;
