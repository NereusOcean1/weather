import React from 'react';
import { observer } from 'mobx-react-lite';
import { Menu, Button } from '@mantine/core';

import { t } from '../../core/language/translations';
import { City } from '../../core/api/rest/Openweathermap/openweathermap.model';
import WeatherStore from '../../core/store/weather/weather.store';
import ConfigStore from '../../core/store/config/config.store';

export const CityMenu: React.FC = observer(() => {
    const { locale } = ConfigStore;

    return (
        <Menu>
            <Menu.Target>
                <Button variant={'outline'} color={'white'}>
                    {t(WeatherStore.city)}
                </Button>
            </Menu.Target>
            <Menu.Dropdown>
                <Menu.Item onClick={() => WeatherStore.setCity(City.SaintPetersburg)}>
                    {t(City.SaintPetersburg)}
                </Menu.Item>
                <Menu.Item onClick={() => WeatherStore.setCity(City.London)}>
                    {t(City.London)}
                </Menu.Item>
            </Menu.Dropdown>
        </Menu>
    );
});
