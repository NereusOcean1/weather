import React from 'react';
import { observer } from 'mobx-react-lite';
import style from './DisplaySelectedWeather.module.scss';
import WeatherStore from '../../core/store/weather/weather.store';
import { InfoTabs } from './component/InfoTabs';
import { WeatherPerDayType } from '../../core/store/weather/weather.model';

type Props = {
    weather?: WeatherPerDayType;
};
export const DisplaySelectedWeather: React.FC<Props> = observer(({ weather }) => {
    const { selectedWeatherIndex, weatherList } = WeatherStore;
    const selectedWeather = weather ? weather : weatherList[selectedWeatherIndex];

    if (!selectedWeather) return null;

    return (
        <div className={style['weather-container']}>
            <div className={style['weather-container__content']}>
                <img src={selectedWeather.weather.iconUrl} sizes={'auto'} />
                <p className={style['weather-container__description']}>
                    {selectedWeather.weather.description}
                </p>
                <p className={style['weather-container__temperature']}>
                    {selectedWeather.temperature.temp}
                    {selectedWeather.temperature.symbol}
                </p>
            </div>
            <InfoTabs selectedWeather={selectedWeather} />
        </div>
    );
});
