import React from 'react';
import classNames from 'classnames';

import { FaTemperatureArrowUp, FaTemperatureArrowDown } from 'react-icons/fa6';
import { SlPeople } from 'react-icons/sl';
import { FaWind } from 'react-icons/fa';
import { BsMoisture } from 'react-icons/bs';

import { WeatherPerDayType } from '../../../core/store/weather/weather.model';
import style from './InfoTabs.module.scss';
import CustomPopover from '../../UI/Popover/CustomPopover';
import { t } from '../../../core/language/translations';

type Props = {
    selectedWeather?: WeatherPerDayType;
};
export const InfoTabs: React.FC<Props> = ({ selectedWeather }) => {
    if (!selectedWeather) return null;

    return (
        <div className={style['info-tabs']}>
            <CustomPopover label={t('humidity')}>
                <div className={style['info-tabs__tab']}>
                    <div
                        className={classNames(
                            style['info-tabs__img'],
                            style['info-tabs__img-blue'],
                        )}
                    >
                        <BsMoisture />
                    </div>
                    {selectedWeather.temperature.humidity}%
                </div>
            </CustomPopover>
            <CustomPopover label={t('minTemp')}>
                <div className={style['info-tabs__tab']}>
                    <div
                        className={classNames(style['info-tabs__img'], style['info-tabs__img-red'])}
                    >
                        <FaTemperatureArrowDown />
                    </div>
                    {selectedWeather.temperature.temp_min}
                    {selectedWeather.temperature.symbol}
                </div>
            </CustomPopover>
            <CustomPopover label={t('maxTemp')}>
                <div className={style['info-tabs__tab']}>
                    <div
                        className={classNames(
                            style['info-tabs__img'],
                            style['info-tabs__img-green'],
                        )}
                    >
                        <FaTemperatureArrowUp />
                    </div>
                    {selectedWeather.temperature.temp_max}
                    {selectedWeather.temperature.symbol}
                </div>
            </CustomPopover>
            <CustomPopover label={t('feelsLike')}>
                <div className={style['info-tabs__tab']}>
                    <div
                        className={classNames(
                            style['info-tabs__img'],
                            style['info-tabs__img-green'],
                        )}
                    >
                        <SlPeople />
                    </div>
                    {selectedWeather.temperature.feels_like}
                    {selectedWeather.temperature.symbol}
                </div>
            </CustomPopover>
            <CustomPopover label={t('windSpeed')}>
                <div className={style['info-tabs__tab']}>
                    <div
                        className={classNames(
                            style['info-tabs__img'],
                            style['info-tabs__img-blue'],
                        )}
                    >
                        <FaWind />
                    </div>
                    {selectedWeather.wind.speed}
                    {selectedWeather.wind.symbol}
                </div>
            </CustomPopover>
        </div>
    );
};
