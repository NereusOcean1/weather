import React from 'react';
import { observer } from 'mobx-react-lite';
import { Menu, Button } from '@mantine/core';
import ConfigStore from '../../core/store/config/config.store';
import { AppLocale } from '../../core/language/Locale.model';
export const LanguageMenu: React.FC = observer(() => {
    const { locale, setLocale } = ConfigStore;

    // Helper function to get the label for the current locale
    const getLocaleLabel = (locale: AppLocale) => {
        switch (locale) {
            case AppLocale.RU:
                return 'Русский';
            case AppLocale.EN:
                return 'English';
            default:
                return 'Unknown';
        }
    };

    return (
        <Menu>
            <Menu.Target>
                <Button variant={'outline'} color={'white'}>
                    {getLocaleLabel(locale)} {/* Display the label of the current locale */}
                </Button>
            </Menu.Target>
            <Menu.Dropdown>
                <Menu.Item onClick={() => setLocale(AppLocale.RU)}>Русский</Menu.Item>
                <Menu.Item onClick={() => setLocale(AppLocale.EN)}>English</Menu.Item>
            </Menu.Dropdown>
        </Menu>
    );
});
