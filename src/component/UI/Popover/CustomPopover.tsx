import { Popover, Text } from '@mantine/core';
import { useDisclosure } from '@mantine/hooks';
import React, { ReactElement } from 'react';

type Props = {
    targetClassName?: string;
    offset?: number;
    label: string;
    children: ReactElement;
};
const CustomPopover: React.FC<Props> = ({ children, label, targetClassName, offset }) => {
    const [opened, { close, open }] = useDisclosure(false);

    return (
        <Popover position="top" withArrow opened={opened} offset={offset}>
            <Popover.Target>
                <div className={targetClassName} onMouseEnter={open} onMouseLeave={close}>
                    {children}
                </div>
            </Popover.Target>
            <Popover.Dropdown>
                <Text size="sm" fw={'bold'}>
                    {label}
                </Text>
            </Popover.Dropdown>
        </Popover>
    );
};
export default CustomPopover;
