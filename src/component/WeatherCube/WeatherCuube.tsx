import React, { useEffect, useRef } from 'react';
import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import WeatherStore from '../../core/store/weather/weather.store';
import { WeatherPerDayType } from '../../core/store/weather/weather.model';

function drawCenteredText(
    context: CanvasRenderingContext2D,
    text: string,
    y: number,
    canvasWidth: number,
) {
    const textWidth = context.measureText(text).width;
    context.fillText(text, (canvasWidth - textWidth) / 2, y);
}

const createWeatherTexture = async (weatherData: WeatherPerDayType) => {
    const canvas = document.createElement('canvas');
    canvas.width = 256;
    canvas.height = 256;
    const context = canvas.getContext('2d');
    if (!context) throw new Error('Failed to get 2D context');

    context.fillStyle = '#000000';
    context.fillRect(0, 0, canvas.width, canvas.height);

    context.fillStyle = '#ffffff';
    context.font = '14px Arial';

    drawCenteredText(context, weatherData.city.name, 20, canvas.width);
    drawCenteredText(
        context,
        `${weatherData.temperature.temp}${weatherData.temperature.symbol}`,
        240,
        canvas.width,
    );
    drawCenteredText(context, weatherData.date, 200, canvas.width);

    // Load image
    if (weatherData.weather.iconUrl) {
        return new Promise<THREE.Texture>((resolve, reject) => {
            const img = new Image();
            img.onload = () => {
                context.drawImage(img, (canvas.width - 50) / 2, (canvas.height - 50) / 2, 50, 50);
                resolve(new THREE.CanvasTexture(canvas));
            };
            img.onerror = reject;
            img.src = weatherData.weather.iconUrl ? weatherData.weather.iconUrl : '';
        });
    } else {
        return Promise.resolve(new THREE.CanvasTexture(canvas));
    }
};

const WeatherCube: React.FC = () => {
    const mountRef = useRef<HTMLDivElement>(null);

    useEffect(() => {
        if (!mountRef.current) return;

        const scene = new THREE.Scene();
        const camera = new THREE.PerspectiveCamera(
            75,
            window.innerWidth / window.innerHeight,
            1,
            1000,
        );

        const renderer = new THREE.WebGLRenderer();
        renderer.setSize(window.innerWidth / 1.1, window.innerHeight / 1.1);
        renderer.setClearColor(0x000000, 0);
        mountRef.current.appendChild(renderer.domElement);

        const controls = new OrbitControls(camera, renderer.domElement);
        camera.position.set(0, 0, window.innerHeight / 100);
        controls.update();

        const light = new THREE.PointLight(0xffffff, 1, 100);
        light.position.set(10, 10, 10);
        scene.add(light);

        const ambientLight = new THREE.AmbientLight(0x404040); // soft white light
        scene.add(ambientLight);

        Promise.all(WeatherStore.weatherList.map(createWeatherTexture))
            .then(textures => {
                const geometry = new THREE.BoxGeometry(3, 3, 3);
                const materials = textures.map(
                    texture => new THREE.MeshBasicMaterial({ map: texture }),
                );
                const cube = new THREE.Mesh(geometry, materials);
                scene.add(cube);

                const animate = () => {
                    requestAnimationFrame(animate);
                    controls.update();
                    renderer.render(scene, camera);
                };

                animate();
            })
            .catch(console.error);

        return () => {
            if (mountRef.current) mountRef.current.removeChild(renderer.domElement);
        };
    }, []);

    return <div ref={mountRef} style={{ height: '80vh', width: '100vw' }}></div>;
};

export default WeatherCube;
