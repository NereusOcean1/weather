import React from 'react';
import { observer } from 'mobx-react-lite';
import style from './WeatherList.module.scss';
import WeatherStore from '../../core/store/weather/weather.store';
import classNames from 'classnames';

const WeatherList = observer(() => {
    const { weatherList, isLoading, setSelectedWeather, selectedWeatherIndex } = WeatherStore;

    if (isLoading) {
        return <div>Loading...</div>;
    }

    if (!weatherList.length) {
        return <div>No weather data available.</div>;
    }

    return (
        <div className={style['weather-list']}>
            {weatherList.map((day, index) => (
                <div
                    key={day.date}
                    className={classNames(style['weather-list__tab'], {
                        [style['weather-list__tab-selected']]: selectedWeatherIndex === index,
                    })}
                    onClick={() => setSelectedWeather(index)}
                >
                    <p>{day.date}</p>
                    <img
                        src={day.weather.iconUrl}
                        alt={day.weather.description}
                        className={style['weather-list__img']}
                    />
                    <p>
                        {day.temperature.temp_max} / {day.temperature.temp_min}
                        {day.temperature.symbol}
                    </p>
                </div>
            ))}
        </div>
    );
});

export default WeatherList;
