import { getWeather, getWeatherIcons } from './rest/Openweathermap/openweathermap';

export { getWeather, getWeatherIcons };
