export type MakeRequestType = {
    method?: RequestMethod;
    url: string;
    data?: any;
    responseType?: any;
};

export enum RequestMethod {
    Get = 'get',
    Post = 'post',
}
