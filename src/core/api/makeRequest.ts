import axios from 'axios';
import { MakeRequestType, RequestMethod } from './makeRequest.module';

export const makeRequest = ({
    method = RequestMethod.Get,
    url = '/',
    data,
    responseType,
}: MakeRequestType) => {
    return axios({ method, url, data, responseType });
};
