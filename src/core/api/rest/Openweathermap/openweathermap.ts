import { makeRequest } from '../../makeRequest';
import { RequestMethod } from '../../makeRequest.module';
import { City } from './openweathermap.model';
import { AppLocale } from '../../../language/Locale.model';

const APIKEY = 'f7029b07fe60e91f95f3d83bb7fcf1b6';
const WEATHER_BASE_URL = 'https://api.openweathermap.org/data/2.5/forecast';
const WEATHER_BASE_ICON_URL = 'https://openweathermap.org/img/wn';

type dataType = {
    countDays?: number;
    city?: City;
    lang?: AppLocale;
};
export const getWeather = ({
    countDays = 1,
    city = City.SaintPetersburg,
    lang = AppLocale.RU,
}: dataType) => {
    return makeRequest({
        method: RequestMethod.Get,
        url: `${WEATHER_BASE_URL}?q=${city}&cnt=${countDays}&lang=${lang}&appid=${APIKEY}`,
    });
};

type WeatherIconType = {
    icon?: string;
};
export const getWeatherIcons = ({ icon }: WeatherIconType) => {
    return makeRequest({
        method: RequestMethod.Get,
        url: `${WEATHER_BASE_ICON_URL}/${icon}@2x.png`,
        responseType: 'blob',
    });
};
