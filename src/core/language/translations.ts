import { I18n, TranslateOptions } from 'i18n-js';

import { AppLocale } from './Locale.model';
import appRuTranslations from './translations/LanguageModule/app.ru.translations';

let translations: any = [appRuTranslations];
const DefaultLocale = AppLocale.RU;

const combineTranslations = <L extends string, K extends string, V extends string>(
    translations: Array<Record<L, Partial<Record<K, unknown>>>>,
) => {
    const result: Record<string, Record<string, unknown>> = {};
    translations.forEach(translation => {
        Object.entries(translation as Record<string, Record<string, unknown>>).forEach(
            ([locale, trans]) => {
                result[locale] = { ...result[locale], ...trans };
            },
        );
    });
    return result as Record<L, Record<K, V>>;
};

let combinedTranslations = combineTranslations(translations);

export type ITranslations = typeof combinedTranslations;

export let LocaleInstance = new I18n(combinedTranslations, {
    enableFallback: true,
    defaultLocale: DefaultLocale,
    locale: DefaultLocale,
});

export const setLocaleInstance = async (locale = DefaultLocale) => {
    const appTranslations = await import(
        `./translations/LanguageModule/app.${locale}.translations`
    );
    translations = [appTranslations.default];
    combinedTranslations = combineTranslations(translations);
    LocaleInstance.translations = combinedTranslations;
    LocaleInstance.locale = locale;
};

export const t = (scope: keyof ITranslations[keyof ITranslations], opts?: TranslateOptions) =>
    LocaleInstance.translate(scope, opts);
