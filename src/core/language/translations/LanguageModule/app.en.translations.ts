const appEnTranslations = {
    en: {
        weather: 'Weather',
        windSpeed: 'Wind speed',
        direction: 'Direction',
        ['Saint Petersburg']: 'Saint Petersburg',
        london: 'London',
        today: 'Today',
        language: 'Language',
        city: 'City',
        maxTemp: 'Maximum temperature',
        minTemp: 'Minimum temperature',
        humidity: 'Humidity',
        feelsLike: 'Feels like',
        cube: 'Cube',
    },
};
export default appEnTranslations;
