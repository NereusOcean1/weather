const appRuTranslations = {
    ru: {
        weather: 'Погода',
        windSpeed: 'Скороcть ветра',
        direction: 'Направление',
        ['Saint Petersburg']: 'Санкт-Петербург',
        london: 'Лондон',
        today: 'Сегодня',
        language: 'Язык',
        city: 'Город',
        maxTemp: 'Максимальная температура',
        minTemp: 'Минимальная температура',
        humidity: 'Влажность',
        feelsLike: 'Ощущается как',
        cube: 'Куб',
    },
};
export default appRuTranslations;
