import { makeAutoObservable } from 'mobx';
import { AppLocale } from '../../language/Locale.model';
import { setLocaleInstance } from '../../language/translations';

class Config {
    locale: AppLocale = AppLocale.RU;
    constructor() {
        makeAutoObservable(this);
    }

    setLocale = async (locale: AppLocale) => {
        await setLocaleInstance(locale);
        this.locale = locale;
    };
}

export default new Config();
