import { IoIosRainy } from 'react-icons/io';
import { RiSnowyFill, RiCloudWindyFill } from 'react-icons/ri';
import { IoPartlySunny, IoSunnySharp } from 'react-icons/io5';
export enum WeatherTypes {
    Rainy = 'rainy',
    Cloudy = 'cloudy',
    Snowy = 'snowy',
    Windy = 'windy',
    Sunny = 'sunny',
}
export const WeatherIcon = {
    [WeatherTypes.Rainy]: <IoIosRainy />,
    [WeatherTypes.Cloudy]: <IoPartlySunny />,
    [WeatherTypes.Snowy]: <RiSnowyFill />,
    [WeatherTypes.Windy]: <RiCloudWindyFill />,
    [WeatherTypes.Sunny]: <IoSunnySharp />,
};

export type TemperatureType = {
    temp: number;
    feels_like: number;
    temp_min: number;
    temp_max: number;
    pressure: number;
    humidity: number;
    symbol: string;
};

export type CityType = {
    id: number;
    sunrise: number;
    sunset: number;
    timezone: number;
    name: string;
    country: string;
    population: string;
    coord: {
        lat: number;
        lon: number;
    };
};

export type WindType = {
    speed: number;
    gust: number;
    deg: number;
    symbol: string;
};

type WeatherDescriptionsType = {
    text: string;
    description: string;
    icon: string;
    iconUrl?: string;
};

export type WeatherPerDayType = {
    date: string;
    city: CityType;
    wind: WindType;
    temperature: TemperatureType;
    weather: WeatherDescriptionsType;
};
