import { WeatherPerDayType } from './weather.model';
import { formatDate, translateUnitsSymbol, unitsByLocal } from '../../../utils/unitsByLocal';
import { UnitsType } from '../../../utils/utils.model';
import { AppLocale } from '../../language/Locale.model';

export const processingWeatherList = (weatherInfo: any, locale: AppLocale) => {
    let prevDate = '';
    const weatherList = weatherInfo.list.map((day: any, index: number) => {
        const { dt, weather, main, wind } = day;

        const currDate = formatDate(dt, locale);
        if (prevDate === currDate) return '';
        prevDate = currDate;

        const weatherPerDay: WeatherPerDayType = {
            date: currDate,
            city: weatherInfo.city,
            weather: {
                ...weather[0],
                icon: weather[0].icon,
                text: weather[0].main,
                description: weather[0].description,
            },
            temperature: {
                ...main,
                temp: unitsByLocal({
                    unit: main.temp,
                    locale: locale,
                    type: UnitsType.Temperature,
                }),
                temp_min: unitsByLocal({
                    unit: main.temp_min,
                    locale: locale,
                    type: UnitsType.Temperature,
                }),
                temp_max: unitsByLocal({
                    unit: main.temp_max,
                    locale: locale,
                    type: UnitsType.Temperature,
                }),
                feels_like: unitsByLocal({
                    unit: main.feels_like,
                    locale: locale,
                    type: UnitsType.Temperature,
                }),
                symbol: translateUnitsSymbol(locale, UnitsType.Temperature),
            },
            wind: {
                ...wind,
                speed: unitsByLocal({
                    unit: wind.speed,
                    locale: locale,
                    type: UnitsType.Speed,
                }),
                symbol: translateUnitsSymbol(locale, UnitsType.Speed),
            },
        };
        return weatherPerDay;
    });

    return weatherList.filter((elem: string) => elem !== '');
};
