import { action, makeAutoObservable, reaction, runInAction } from 'mobx';
import { getWeather, getWeatherIcons } from '../../api';
import { WeatherPerDayType } from './weather.model';
import ConfigStore from '../config/config.store';
import { City } from '../../api/rest/Openweathermap/openweathermap.model';
import { processingWeatherList } from './weather.service';
import weatherList from '../../../component/WeatherList/WeatherList';

class Weather {
    rawData: any;
    weatherList: WeatherPerDayType[] = [];
    selectedWeatherIndex: number = 0;
    city: City = City.SaintPetersburg;
    countDays: number = 0;
    isLoading: boolean = false;
    iconCache = new Map();

    constructor() {
        makeAutoObservable(this);
        this.fetchWeather();
        this.setupReactions();
    }

    setupReactions() {
        reaction(
            () => this.city,
            () => this.fetchWeather(),
        );
        reaction(
            () => ConfigStore.locale,
            () => this.processWeatherData(),
        );
    }

    async fetchWeather() {
        this.isLoading = true;
        try {
            const { locale } = ConfigStore;
            const weatherInfo = await getWeather({
                countDays: this.countDays,
                city: this.city,
                lang: locale,
            });
            runInAction(() => {
                this.rawData = weatherInfo.data;
                this.processWeatherData();
                this.isLoading = false;
            });
        } catch (e) {
            runInAction(() => {
                this.isLoading = false;
            });
        }
    }

    processWeatherData() {
        const { locale } = ConfigStore;
        this.weatherList = processingWeatherList(this.rawData, locale);
        this.loadIconsIfNeeded();
    }

    setSelectedWeather = (index: number) => {
        this.selectedWeatherIndex = index;
    };

    @action
    async loadIconsIfNeeded() {
        for (const weatherDay of this.weatherList) {
            const iconId = weatherDay.weather.icon;
            if (!this.iconCache.has(iconId)) {
                const iconUrl = await this.loadIcon(iconId);
                runInAction(() => {
                    this.iconCache.set(iconId, iconUrl);
                });
            }
            weatherDay.weather.iconUrl = this.iconCache.get(iconId);
        }
    }

    async loadIcon(iconName: string) {
        try {
            const response = await getWeatherIcons({ icon: iconName });
            return URL.createObjectURL(response.data);
        } catch (e) {
            console.error('Failed to load icon:', e);
            return '';
        }
    }

    @action
    setCity(city: City) {
        console.log(city);
        this.city = city;
    }

    @action
    setCountDays(countDays: number) {
        this.countDays = countDays;
    }
}

export default new Weather();
