import React, { useState } from 'react';
import style from './MainScreen.module.scss';
import { DisplaySelectedWeather } from '../../component/DisplaySelectedWeather/DisplaySelectedWeather';
import { observer } from 'mobx-react-lite';
import { CityMenu } from '../../component/CityMenu/CityMenu';
import { Button, Flex, Group, Loader } from '@mantine/core';
import WeatherStore from '../../core/store/weather/weather.store';
import WeatherList from '../../component/WeatherList/WeatherList';
import { LanguageMenu } from '../../component/LanguageMenu/LanguageMenu';
import WeatherCube from '../../component/WeatherCube/WeatherCuube';
import { useDisclosure } from '@mantine/hooks';
import { t } from '../../core/language/translations';
export const MainScreen = observer(() => {
    const [opened, toogleOpened] = useDisclosure(false);

    return (
        <div className={style['main__container']}>
            <Group gap={8}>
                <CityMenu />
                <LanguageMenu />
                <Button variant={'outline'} color={'white'} onClick={toogleOpened.toggle}>
                    {t('cube')}
                </Button>
            </Group>

            <div className={style['main__content']}>
                {WeatherStore.isLoading ? (
                    <Flex align={'center'} justify={'center'} w={400} h={400}>
                        <Loader />
                    </Flex>
                ) : opened ? (
                    <WeatherCube />
                ) : (
                    <>
                        <DisplaySelectedWeather />
                        <WeatherList />
                    </>
                )}
            </div>
        </div>
    );
});
