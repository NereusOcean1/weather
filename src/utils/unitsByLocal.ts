import { AppLocale } from '../core/language/Locale.model';
import { UnitsSymbol, UnitsType } from './utils.model';

export const unitsByLocal = ({
    locale,
    unit,
    type,
}: {
    locale: AppLocale;
    unit: number;
    type: UnitsType;
}) => {
    switch (type) {
        case UnitsType.Temperature:
            return Math.round(
                locale === AppLocale.EN ? kelvinToFarenheit(unit) : kelvinToCelsius(unit),
            );
        default:
            return (locale === AppLocale.EN ? meterPerSecondToMilesPerHour(unit) : unit).toFixed(2);
    }
};

export const translateUnitsSymbol = (locale: AppLocale, type: UnitsType) => {
    if (type === UnitsType.Temperature) {
        return locale === AppLocale.EN ? UnitsSymbol.Farenheit : UnitsSymbol.Celsius;
    } else if (type === UnitsType.Speed) {
        return locale === AppLocale.EN ? UnitsSymbol.MPH : UnitsSymbol.MPS;
    }
};

const kelvinToCelsius = (kelvinTemp: number) => {
    return kelvinTemp - 273.15;
};

const kelvinToFarenheit = (kelvinTemp: number) => {
    return (kelvinTemp - 273.15) * (9 / 5) + 32;
};

const meterPerSecondToMilesPerHour = (meter: number) => {
    return meter * 2.237;
};

export const formatDate = (seconds: number, locale: AppLocale) => {
    const date = new Date(seconds * 1000);
    return date.toLocaleDateString(locale);
};
