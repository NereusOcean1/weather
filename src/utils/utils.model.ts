export enum UnitsType {
    Temperature = 'temperature',
    Speed = 'speed',
    Time = 'time',
}

export enum UnitsSymbol {
    Farenheit = '°F',
    Celsius = '°C',
    MPS = 'm/s',
    MPH = 'mi/h',
}
